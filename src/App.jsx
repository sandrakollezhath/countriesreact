import { useState } from 'react'
import { useEffect } from 'react'
import './App.css'
import Header from './Header'
import Card from './Card'

function App() {
  const [countries, setCountries] = useState([])

  useEffect(
    () => {
      
      fetch("https://restcountries.com/v3.1/all")
        .then(response => response.json())
        .then(data => {
          console.log(data)
          setCountries(data);
        })
        .catch(err => {
          console.log(err)
        })
    },
  );

  return (
    <>
      <Header/>
      <main className="main">
        <section className="CountriesSection">
          <h1 className="CountriesSectionHeading">Country List</h1>
          {countries.map(country =>{
                return <Card countryName={country.name.common}  flagUrl={country.flags.svg}/>
              }) }
        </section>
      </main>
      <footer className="footer"></footer>
    </>
  )
}

export default App
