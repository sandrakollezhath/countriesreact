function Card(props){
    return (
        <article className="list-item">
                <img className="list-img" src={props.flagUrl} alt="countryFlag"/>
                <h2 className="list-span">{props.countryName}</h2>
        </article>
    );
}

export default Card;